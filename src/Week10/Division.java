package Week10;

public class Division implements CalculatorOperation {
    private double left;
    private double right;
    private double result;

    
    public void perform() {
        if (right != 0) {
            result = left / right;
        }
    }
}
